#include <gmp.h>
#include <gmpxx.h>
#include <mpfr.h>

#include <iostream>
#include <cstdint>
#include <cstdlib>
#include <chrono>

#define ROUNDING MPFR_RNDN
#define PRECISION (1024*16)

class calculator_bastian{
        public:
                mpfr_t m_sqrt5;
                mpfr_t m_phi;
                mpfr_t m_tmp;

        calculator_bastian(){
                mpfr_init2(m_phi, PRECISION);
                mpfr_init2(m_sqrt5, PRECISION);
                mpfr_init2(m_tmp, PRECISION);

                mpfr_sqrt_ui(m_sqrt5, 5, ROUNDING);

                mpfr_add_si(m_phi, m_sqrt5, 1, ROUNDING);
                mpfr_div_ui(m_phi, m_phi, 2, ROUNDING);

        }

        void fib(mpz_class &result, uint32_t nr){
                mpfr_pow_ui(this->m_tmp, this->m_phi, nr, ROUNDING);
                mpfr_div(this->m_tmp, this->m_tmp, this->m_sqrt5, ROUNDING);
                mpfr_get_z(result.get_mpz_t(), this->m_tmp, ROUNDING);
        }

        ~calculator_bastian(){
                mpfr_clear(this->m_phi);
                mpfr_clear(this->m_sqrt5);
                mpfr_clear(this->m_tmp);
        }
};

class calculator_naive{
        public:
                mpz_class m_i;

                mpz_class prev = 0;
                mpz_class prevprev = 1;


        void fib(mpz_class &result, uint32_t nr){
                prev = 1;
                prevprev = 1;


                {
                        for(uint32_t i=3;i<=nr;i++){
                                result = prev + prevprev;
                                prevprev = prev;
                                prev = result;
                        }
                }
        }
};

int main(){


        calculator_bastian calc_bas;
        calculator_naive calc_naive;

        mpz_class res;
        mpz_class res2;

        std::cout << "Fibonacci No.;time(bastian);time(naive)" << std::endl;

        for(uint32_t i=3;i<2000000;i++){
                std::chrono::duration<double, std::milli> elapsed, elapsed2;

                std::cout << i << ";";
                {
                        auto start = std::chrono::high_resolution_clock::now();
                        calc_bas.fib(res2, i);
                        auto finish = std::chrono::high_resolution_clock::now();
                        elapsed2 = finish - start;
                        std::cout << elapsed2.count() << ";";
                }

                {
                        auto start = std::chrono::high_resolution_clock::now();
                        calc_naive.fib(res, i);
                        auto finish = std::chrono::high_resolution_clock::now();
                        elapsed = finish - start;
                        std::cout << elapsed.count() << ";" << std::endl;
                }

                if(res != res2){
                        std::cout << "mismatch!" << std::endl
                                  << "Bastians result: " << res2 << std::endl
                                  << "Naive result:" << res << std::endl;
                        break;
                }
        }
        std::cout << std::endl;

        return 0;
}